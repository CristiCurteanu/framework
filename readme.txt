Documentation

Introduction:
This is a basic MVC framework written on PHP, for tutorial purposes only.
It will have no name or description, and the single reason it have been created, is showing the author's PHP coding 
style. The simplicity and lack of security implemented could not allow other programmers to use it in application building (or 
it could not be good enough).

Installation:
  - Download the zip archive with the framework;
  - Unzip the archive and rename it to <your_app_name>;
  - Copy/Move the folder to application directory of your web server;
  - Access http://127.0.0.1/<your_app_name>/?q=welcome/index to check that system have been installed;

Specifications:
For building models, controllers, and views, this framework have built-in functions (object's methods) that are included 
in libraries files, that are all in libs directory. The parent of all libraries is FLoader class, which contains methods
that are used in other child classes of library objects.

FControllers class contains methods for use in Controllers directory, and will contain the actions of future applications;
The methods of this library class are:
  - $this->view(view_file_name, [optional_parameter]) => this method loads a view from views directory; arguments for this 
    function are :
      - view_file_name => only view's filename, without extension;
      - optional_parameter => is a value that will be passed to the view;
  
  - $this->model(model_file_name, [optional_parameter]) => this method loads a model from site/models directory; arguments
    for this function are:
      - model_file_name => indicates the model filename, without extension;
      - optional_parameter => sends a parameter to model that is used;
  
  - $this->upload(file, permitted_extension) => this method could be used when we need to upload a file, with only needed extension;
    The arguments for this function are:
      - file => the target file that is needed to be uploaded;
      - permitted_extension => the extension of file that will need to be uploaded. If the extension will be not right, 
        the method will return 'false';
  
  - $this->load_helper(helper_name, [args]) => this method loads a helper file that contains helper function with
    'helper_name' name; The arguments of this function are:
      - helper_name => this will indicate to load a function from helpers directory, that from <helper_name>.php file, and
      function's name will be helper_name; This means that only a helper file from helpers directory, will contain a single
      custom function;
      - args => in case the helper function have arguments, they could be passed via this method as optional arguments;
  
  - $this->load_custom_library(library_name) => this method returns an object, which is actually a helper module, that
    contains more than one helper functions;
      - library_name => indicates the name of file that contains the helper object, and the object's name;

FLoader class contains methods that can be used everywhere in the application. It is the highest level parent class, and 
other libraries (or other classes) inherit it's methods, for common use; The methods that contains this class are:
  - this->main_url() => This is the method that is used for reading url of the application, and calls the objects and methods from controllers files;

  - $this->load(file, class, [function]) => method that is used to return a object, or a function of an object that are included
    in a certain file; Arguments of this method are:
      - file => file it is need to be loaded;
      - class => the name of class it is need to be returned;
      - function => the name of function it is need to be returned (optional);

  - $this->directory(directory, filename) => this function returns a string that indicates a path for a certain php file that is used 
    in the project; The arguments are:
      - directory => indicates the path in application directory that contains the required file;
      - filename => indicates the filename of the required file;

  - $this->assets_path(filename, option) => this function loads assets that are included in site/assets directory;
    The arguments for this function are:
      - filename => indicate the filename of the asset file;
      - option => indicate filetype of the asset file, and indicates from which directory to load it; The options could be only:
        -> css => indicates to assets/css directory which contains .css files;
        -> js => indicates to assets/js directory which contains .js files;
        -> images => indicates to assets/images directory which contains image files;

  - $this->redirect_to(Link_text, controller, function) => this method creates a link to a page that is included in a 'controller' 
  and 'function'; The arguments are:
    - Link_text => This string will appear as text for link;
    - controller => indicates which controller file and class to access;
    - function => indicates which function of selected controller to access. Default value is 'index';

  - $this->to_root() => this function creates a link that redirect to home/root page; The root directory controller+function
    could be set up in the settings/route-settings.php file;  
  
  - $this->db_settings() => method used to load all the database connection settings that are included in 
    /settings/db-settings.php file;

  - $this->pass_args(arg, file) => method used to pass arguments to a certain file (ussually a view file);

FModel class is a library that contain methods for Active Records, which allow to interacte with database;
The methods that are in this class are:
  - $this->dbConnect() => this function uses parameters that are included in settings/db-setting.php file, and uses them to
    make a connection with the database via db_settings() method of FLoader object;
  
  - $this->dbSelect() => this function(is optional) choose a database to work with;
  
  - $this->find(table) => this function return all data from all column in the 'table' table; The argument :
    - table => indicates to which table of database to connect and return the data;

  - $this->select(table, column) => this function select certain column from 'table' table; The arguments are :
    - table => indicates from which table the data will be selected;
    - column => indicates which columns will be selected; 

  - $this->find_where(table, condition) => this function returns all data from a table with a certain condition, which means
    there will be used 'WHERE' conditions; The arguments are :
    - table => indicates from which table the data will be selected;
    - condition => the condition used for downloading data;

  - $this->find_where_or(table, condition1, condition2) => this function returns all data from a table executing one of this 
    condition; The query is using OR keyword; The arguments of this function are:
      - table => indicates from which table the data will be selected;
      - condition1, condition2 => when one of the condition or both conditions are satisfied, the query is executed;

  - $this->find_where_and(table, condition1, condition2) => this function returns all data from a table when both conditions
    are satisfied; The query is using AND keyword; The arguments of this functions are:
      - table => indicates from which table the data will be selected;
      - condition1, condition2 => when both condition1 and condition2 are satisfied, the query is executed;

  - $this->join(table, arg1, arg2, join_type) => this function execute the 'JOIN' query, and adds arg1 to arg2 of the same table;
    The arguments are:
      - table => indicates from which table the data will be selected;
      - arg1, arg2 => indicates which columns to join;
      - join_type => indicates what type of 'JOIN' will be used; There could be only this values:
        -> left;
        -> right;
        -> full;
        -> inner;

  - $this->insert(table, data) => this function will insert new 'data' into 'table' of database; The arguments are:
      - table => indicates to which table to add the data;
      - data => array with all the data needed to be inserted into database;

  - $this->update(table, current_data, update_data) => this function is updating 'current_data' to 'update_data' in the 
    'table'; The arguments are:
      - table => indicates in which table the data will be updated;
      - current_data => search for data in the table to be updated;
      - update_data => update current_data with new updated_data;

FViews class is the library that contains the methods for the views; The methods of this object are:
  - $this->render(header, footer, content, [file_path]) => this method is used for rendering a page by using pieces of html code, 
    from different files, and create a single html file;