<?php 
  /**
  * The index controller
  */
  class Index extends FController
  {
    function show() {
      $array = array('FirstItem','SecondItem','ThirdItem');
      $this->view('index', $array);
    }
  }
?>