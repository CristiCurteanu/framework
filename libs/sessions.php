<?php 

  class FSessions extends FLoader
  {
    function init() {
      session_start();
    }

    function set($key, $val) {
      $_SESSION[$key] = $val;
    }

    function get($key) {
      if (isset($_SESSION[$key])) {
        return $_SESSION[$key];
      }
    }

    function destroy() {
      session_destroy();
    }
  }
?>