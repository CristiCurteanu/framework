<?php 
  //Include all the library files
  $files = glob(dirname(__DIR__)."/libs/*.php");
  foreach ($files as $file) {
    if ($file != __FILE__ && include $file) {
    } 
  }
  //Include all the settings files
  $configs = glob(dirname(__DIR__)."/settings/*.php");
  foreach ($configs as $config) {
    if (!(include $config)) {
      echo "The file ".$config."was not included";
      continue;
    }
  }

  class FLoader
  {    
    function main_url() {
      //Creates the url adress;
      $q = $_GET['q'];
      $q = rtrim($q, '/');
      $limit = 2;
      global $home;

      // Check if url is empty, and redirect it to main page
      if (empty($_GET['q'])) {
        $url[0] = $home['controller'];
        $url[1] = $home['function'];
        $file1 = 'site/controllers/'.$url[0].'.php';
        if (file_exists($file1)) {
          require $file1;
        }
        $controller = new $url[0];
        return $controller->{$url[1]}();
      }      
      //Permite crearea unui url ce permite accesarea doar a controller-ului si a unei metode;
        else {
          $url = explode('/', $q, $limit);
          // print_r($url);
          $file = 'site/controllers/'.$url[0].'.php';
          if (file_exists($file)) {
            require $file;
          } else {
            throw new Exception("Error Processing Request. The file $file does not exits!");
          }

          $controller = new $url[0];
          if (isset($url[1])) {
            $controller->{$url[1]}();
          } 

          if (isset($url[2])) {
            $controller->{$url[1]}($url[2]);
          }        
      }
    } 

    function to_root($text)
    {
      global $home;
      global $basepath;
      $file2 = 'site/controllers/'.$url[0].'.php';
      if (file_exists($file2)) {
        require $file2;
      }
      // $controller = new $url[0];
      // return $controller->{$url[1]}();
      echo "<a href=\"".$basepath.$home['controller']."/".$home['function']."\">".$text."</a>";
      // echo "<a href=\"".$basepath.$controller."/".$function."\">".$link_text."</a>";
    }

    // Return a called object
    function load($file, $class, $function = null) {
      include $file;
      $obj = new ucfirst($class);
      if ($function != null) {
        return $obj->{$function}();
      } else {
        return $obj;
      }
    }
    //Return paths
    function directory($app_dir, $file_name) {
      return dirname(__DIR__)."/".$app_dir."/".$file_name.".php";
    }

    // It can be used between css, js, image
    function assets_path($file_name, $option) {
      $basic_dir = dirname(__DIR__)."/site/assets/";
        switch ($option) {
          case 'css':
            if (file_exists($basic_dir."css/".$file_name)) {
              $asset = $basic_dir."css/".$file_name;
            }
            break;

          case 'js':
            if (file_exists($basic_dir."js/".$file_name)) {
              $asset = $basic_dir."js/".$file_name;
            }
            break; 

          case 'images':
            if (file_exists($basic_dir."images/".$file_name)) {
              $asset = $basic_dir."images/".$file_name;
            }
            break; 
          
          default:
            if (file_exists($basic_dir.$file_name)) {
              $asset = $basic_dir.$file_name;
            } else {
              die("The are no such file! Check the function's arguments or if the file is included;");
            }
            break;
        }
      return $asset; 
    }

    function redirect_to($link_text,$controller,$function = nil) {
      global $basepath;
      if ($function != nil) {
        echo "<a href=\"".$basepath.$controller."/".$function."\">".$link_text."</a>";
      } else {
        $function = 'index';
        echo "<a href=\"".$basepath.$controller."/".$function."\">".$link_text."</a>";
      }
    }

    function db_settings() {
      global $db;
      return $db;
    }

    function pass_args($var, $file) {
      extract($var);
      include $file;
    }
  }
?>