<?php 

class FModels extends FLoader
{ 
  // Function that connect to database and selects the database to work with;
  private function dbConnect() {
    $db_params = $this->db_settings();
    // Check if the db was connected
    return $db_connection = mysqli_connect($db_params['servername'], $db_params['username'], $db_params['password'])
      or die(mysqli_errno()."<br>".'The app can not connect to database.<br>
                                  Check the settings/db-settings.php file to ensure that you have entered the right data');
  }

  // Select a database to work with;
  private function dbSelect() {
    $db_params = $this->db_settings();
    return $db_select = mysqli_select_db($dbConnect(), $db_params['database']) 
      or die('The database have not been found. 
              <br>Check the settings/db-settings.php file to ensure that you have entered the right data');
  }

  // Return an object from a $table;
  function find($table) {
    $query = "SELECT * FROM ".$table;
    return mysqli_query($this->dbConnect(), $query);
  }

  function select($table, $column) {
    $query = "SELECT ".$column." FROM ".$table;
    return mysqli_query($this->dbConnect(), $query);
  }  

  // Return an object from database by aplying 'where' condition;
  function find_where($table, $condition) {
    $query = "SELECT * FROM ".$table." WHERE ".$condition;
    return mysqli_query($this->dbConnect(), $query);
  }

  // Return an object from database by aplying 2 parallel conditions;
  function find_where_or($table, $cond1, $cond2) {
    $query = "SELECT * FROM ".$table." WHERE ".$cond1." OR ".$cond2;
    return mysqli_query($this->dbConnect(), $query);
  }

  // Return an object from database which satysfies 2 conditions;
  function find_where_and($table, $cond1, $cond2) {
    $query = "SELECT * FROM ".$table." WHERE ".$cond1." AND ".$cond2;
    return mysqli_query($this->dbConnect(), $query);
  }

  function join($table, $columns, $opt1, $opt2, $option) {
    $opt = strtolower($option);
    switch ($opt) {
      case 'left':
        $query = "SELECT ".$columns." FROM ".$table." LEFT JOIN ".$opt1." ON ".$opt2;
        break;

      case 'right':
        $query = "SELECT ".$columns." FROM ".$table." RIGHT JOIN ".$opt1." ON ".$opt2;
        break; 

      case 'full':
        $query = "SELECT ".$columns." FROM ".$table." FULL JOIN ".$opt1." ON ".$opt2;
        break;

      case 'inner':
        $query = "SELECT ".$columns." FROM ".$table." INNER JOIN ".$opt1." ON ".$opt2;
        break; 
      
      default:
        return "Choose the right option";
        break;
    }
    return mysqli_query($this->dbConnect(), $query);
  }

  function insert($table, $data) {
    $keys = array_keys($data);
    $values = array_values($data);
    $query = "INSERT INTO ".$table." ".$keys." VALUES ".$values;
    return mysqli_query($this->dbConnect(), $query);
  }

  function update($table, $current_data, $update_data) {
    $query = "UPDATE ".$table." SET ".$update_data." WHERE ".$current_data;
    return mysqli_query($this->dbConnect(), $query);
  }
}
 ?>