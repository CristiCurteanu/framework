<?php 

  class FController extends FLoader
  {
    function view($view, $var = null) {
      //At this step the view file is needed to be loaded
      $view_file = $this->directory("views", $view);
      if ($var != null && file_exists($view_file)) {
        include $view_file;
        extract($var);
        // $this->pass_args($var, $view_file);
      } elseif ($view_file) {
        include $view_file;
      } else {
        die ("Such file does not exist");
      }
    }
    //---------------------------------------------

    function model($model, $var = null) {
      //At this step the model file will be loaded
      $model_file = $this->directory("site/models", $model);
      if ($var != null && file_exists($model_file)) {
        $this->pass_args($var, $model_file);
      } elseif (file_exists($model_file)) {
        include $model_file;
        $modelName = new $model;
      } else {
        die ("Such file does not exist");
      }
    }
    //---------------------------------------------

    // Method used for uploading files; -------------------
    function upload($name, $extension) {
      $target_dir = dirname(__DIR__)."/files";
      $target_file = $target_dir.basename($_FILES["file"][$name]);

      // Check the file extension
      $fileType = pathinfo($target_file, PATHINFO_EXTENSION);
      if (is_array($extension)) {
        foreach ($extension as $e) {
          if ($fileType != $e) {
            die('This is the wrong file format!');
          }         
        }
      } else {
        if ($fileType != $e) {
          die('This is the wrong file format');
        }
      }
      // Check if the file have been uploaded
      if (!(file_exists($target_file))) {
        //upload
        if (copy($_FILES["file"]["tmp_name"], $target_file)) {
              $this->redirect_to('index');
        } else {
          die("File have been not uploaded");
        }
      }      
    }
    //---------------------------------------------

    // Method used for including a helper method, and passing the parameters to that helper;
    function load_helper($helper_name, $arg1 = null, $arg2 = null) {      
      if (include directory('libs/helpers', $helper_name)) {
        if ($arg1 != null) {
           $this->$helper_name($arg1);
        } elseif ($arg1 != null && $arg2 != null) {
          $this->helper_name($arg1, $arg2);
        }
      }
    }
    //---------------------------------------------

    // Method used to load a customized library to application
    function load_custom_library($library_name) {
      $library_file = $this->directory('libs/modules', $library_name);
      if (file_exists($library_file)) {
        $name = ucfirst($library_name);
        $obj = new $name();
      } else {
        die("File with module/custom library have not been found");
      }
      return $obj;
    }  
    //---------------------------------------------
  }
?>