<?php 
  // The class with methods for security inputs
  class FProtection  extends FLoader
  {
    function SecureInput($val)
    {
      if (isset($val)) {
        $text = $this->BlockInjections($val, FILTER_SANITIZE_STRING);
        return $text;
      } else {
        die("There is no text in your textarea");
      }
    }

    private function BlockInjections($val)
    {
      $valid_string = str_replace(array("'","\"","'",'"'), array("&#39;","&quot;","&#39;","&quot;"), $val);
      return $valid_string;
    }

    # other private methods to secure the inputs ...
  }

?>