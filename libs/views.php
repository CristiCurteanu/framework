<?php 
  /**
  * 
  */
  class FViews extends FLoader
  {
    function render($header, $footer, $content, $content_path = null, $arg1 = nil, $arg2 = nil, $arg3 = nil) {
      $header_file = $this->directory('views/layouts',$header);
      $footer_file = $this->directory('views/layouts',$footer); 

      if ($content_path != null) {
        $content_file = $this->directory($content_path, $content);
      } else {
        $content_file = $this->directory('views', $content);
      }   
      $obj = $this->load($this->directory('libs', 'controllers'), 'fController');
      // If all file exists, include all those files, otherwise exit the function
      if (file_exists($header_file) && file_exists($footer_file) && file_exists($content_file)) {
        if ($arg1 != nil) {
          $this->view($header_file, $arg1);
        } elseif ($arg2 != nil) {
          $this->view($header_file, $arg2);
        } elseif ($arg3 != nil) {
          $this->view($header_file, $arg3);
        } else {
          $this->view($header_file);
          $this->view($content_file);
          $this->view($footer_file);          
        }

      } else {
        die("One or more of the view files does not exists. <br> 
             Check the header and footer if they are in layouts folder, and content in main folder <br>
             and the content file are in views folder. You also can give it a customised path as a 4'th argument <br>
             for this function");
      }
      
    }
  }
?>